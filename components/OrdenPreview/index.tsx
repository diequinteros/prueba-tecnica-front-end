import React from 'react';

interface IProperties {
  id: number;
  cliente: string;
  fechaHora: string;
}

const OrdenPreview = ({ id, cliente, fechaHora }: IProperties) => {
  return (
    <div className="flex flex-col sm:flex-row sm:justify-between border border-theme-border-color p-2 rounded-lg hover:bg-theme-orange/10 hover:cursor-pointer">
      <div className="flex flex-col gap-2 ">
        <span className="font-bold underline">Orden #{id}</span>
        <span>Cliente: {cliente}</span>
      </div>
      <span className=" self-end sm:self-auto">{fechaHora}</span>
    </div>
  );
};

export default OrdenPreview;
