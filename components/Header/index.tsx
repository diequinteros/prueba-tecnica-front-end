import { ROUTES } from '@/utils/constants';
import Link from 'next/link';
import React from 'react';

const Header = () => {
  return (
    <div className="p-8 flex flex-row justify-between bg-theme-orange text-white shadow-lg shadow-gray-300">
      <Link href={'/'}>
        <h1 className="font-bold">Prueba tecnica</h1>
      </Link>
      <div className="flex flex-row gap-2 justify-end items-end">
        <Link className="hover:underline" href={ROUTES.ORDENES}>
          Ordenes
        </Link>
      </div>
    </div>
  );
};

export default Header;
