import React from 'react';

interface IProperties {
  show: boolean;
}

const Loading = ({ show }: IProperties) => {
  return (
    show && (
      <div className="theme-full-dark-blur">
        <div className="w-full h-full flex justify-center items-center">
          <span className=" animate-spin text-[8rem] text-theme-orange origin-center text-center leading-normal font-mono">
            ▣
          </span>
        </div>
      </div>
    )
  );
};

export default Loading;
