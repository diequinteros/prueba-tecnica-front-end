import { IDetalle } from '@/types/apiResponse';
import React, { ChangeEvent } from 'react';

interface TProperties extends IDetalle {
  onChange?: (event: ChangeEvent<HTMLInputElement>) => void;
}

const PreviewDetalleItem = ({
  onChange,
  producto: { nombre, descripcion, unidadMedida, precio },
  cantidad,
}: TProperties) => {
  return (
    <div className="flex flex-col sm:flex-row justify-between border-2 border-theme-border-color p-2">
      <div className="flex flex-col gap-1">
        <span className="font-bold">Nombre: {nombre}</span>
        <span>Descripcion: {descripcion}</span>
        <span>Precio unitario: ${precio.toFixed(2)}</span>
      </div>
      <div className="flex flex-col gap-1 self-end items-end">
        <div>
          <label>Cantidad:</label>{' '}
          {onChange === undefined ? (
            <span>{`${cantidad}`}</span>
          ) : (
            <input
              type="number"
              className="w-16"
              max={999}
              min={1}
              value={cantidad}
              onChange={onChange}
            />
          )}
          {unidadMedida}
        </div>
        <span className="font-bold">
          Subtotal: ${(cantidad * precio).toFixed(2)}
        </span>
      </div>
    </div>
  );
};

export default PreviewDetalleItem;
