'use client';
import useCloseContextMenu from '@/hooks/useCloseContextMenu';
import React, { useRef } from 'react';

interface IProperties {
  show: boolean;
  title: string;
  message: string;
  onAccept: () => void;
  onCancel?: () => void;
  onClickOutside?: () => void;
}

const Alert = ({
  show,
  title,
  message,
  onAccept,
  onCancel,
  onClickOutside,
}: IProperties) => {
  const alertReference = useRef<HTMLDivElement>(null);
  useCloseContextMenu(alertReference, onClickOutside ?? (() => {}));
  return (
    show && (
      <div className="theme-full-dark-blur">
        <div className="w-full h-full flex justify-center items-center">
          <div
            className="bg-white border-2 border-theme-border-color rounded-md overflow-hidden w-80 shadow-lg shadow-black/10"
            ref={alertReference}
          >
            <div className="bg-theme-orange p-2">
              <span className="font-bold text-white">{title}</span>
            </div>
            <div className="p-2">
              <p>{message}</p>
            </div>
            <div className="flex flex-row gap-2 justify-end p-2">
              <button onClick={onAccept} className="boton-guardar">
                Aceptar
              </button>
              {onCancel === undefined ? undefined : (
                <button onClick={onCancel} className="boton-error">
                  Cancelar
                </button>
              )}
            </div>
          </div>
        </div>
      </div>
    )
  );
};

export default Alert;
