'use client';
import useCloseContextMenu from '@/hooks/useCloseContextMenu';
import React, {
  DetailedHTMLProps,
  InputHTMLAttributes,
  useEffect,
  useRef,
  useState,
} from 'react';

type TId = string | number;

interface IOption {
  id: TId;
  label: string;
}

interface IProperties
  extends DetailedHTMLProps<
    InputHTMLAttributes<HTMLInputElement>,
    HTMLInputElement
  > {
  options: IOption[];
  selectedOption?: IOption;
  onClickOption: (id: TId) => void;
  onUnselect: () => void;
}

const SelectWithSearch = ({
  options,
  selectedOption,
  onClickOption,
  value,
  disabled,
  onUnselect,
  ...inputProperties
}: IProperties) => {
  const [isFocused, setIsFocused] = useState(false);

  const contextMenuReference = useRef<HTMLDivElement>(null);

  const handleOpen = () => {
    selectedOption ?? setIsFocused(true);
  };

  const handleClose = () => setIsFocused(false);

  useCloseContextMenu(contextMenuReference, handleClose);

  useEffect(() => {
    const handleClickOutsideContextMenu = (event: MouseEvent) => {
      if (
        contextMenuReference.current &&
        event.target instanceof Element &&
        !contextMenuReference.current.contains(event.target)
      ) {
        handleClose();
      }
    };

    document.addEventListener('mousedown', handleClickOutsideContextMenu);

    return () => {
      document.removeEventListener('mousedown', handleClickOutsideContextMenu);
    };
  }, []);
  return (
    <div className="relative w-fit" onClick={handleOpen}>
      <div className="flex flex-row items-center">
        <input
          {...inputProperties}
          value={selectedOption === undefined ? value : selectedOption.label}
          disabled={selectedOption === undefined ? disabled : true}
        />
        <span
          className={`text-lg hover:cursor-pointer hover hover:text-theme-red right-2 ${
            selectedOption === undefined ? 'hidden' : 'absolute'
          }`}
          title="Cancelar seleccion"
          onClick={onUnselect}
        >
          ×
        </span>
      </div>
      <div
        ref={contextMenuReference}
        className={` w-full bg-white border-2 border-theme-border-color rounded-b-md ${
          isFocused && options.length > 0 ? 'flex' : 'hidden'
        } flex-col gap-1 absolute p-1 z-10 shadow-xl shadow-gray-400/20`}
      >
        {options.map((option) => (
          <div
            className="border-2 border-theme-border-color p-1  rounded-md hover:bg-theme-orange/10 hover:cursor-pointer"
            key={option.id}
            onClick={(event) => {
              event.stopPropagation();
              onClickOption(option.id);
              handleClose();
            }}
          >
            {option.label}
          </div>
        ))}
      </div>
    </div>
  );
};

export default SelectWithSearch;
