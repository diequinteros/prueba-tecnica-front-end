export interface ICliente {
  id: number;
  nombre: string;
  apellido: string;
  direccion: string;
  municipio: string;
  departamento: string;
}

export interface IProducto {
  id: number;
  nombre: string;
  precio: number;
  descripcion: string;
  unidadMedida: string;
}

export interface IDetalle {
  id: number;
  producto: IProducto;
  cantidad: number;
}

export interface IFechaHora {
  date: string;
  timezone_type: number;
  timezone: string;
}

export interface IOrden {
  id: number;
  fechaHora: IFechaHora;
  cliente: ICliente;
  detalles: IDetalle[];
}

export interface IResponse<IData> {
  message: string;
  data: IData;
}
