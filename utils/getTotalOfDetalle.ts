import { IDetalle } from '@/types/apiResponse';

const getTotalOfDetalle = (detalles: Omit<IDetalle, 'id'>[]) => {
  let total = 0;

  for (const detalle of detalles) {
    total += detalle.cantidad * detalle.producto.precio;
  }

  return total.toFixed(2);
};

export default getTotalOfDetalle;
