export const validateCantidad = (cantidad: number) => {
  let newCantidad = cantidad;
  if (newCantidad > 999) {
    newCantidad = 999;
  }
  if (newCantidad < 1) {
    newCantidad = 1;
  }
  return newCantidad;
};
