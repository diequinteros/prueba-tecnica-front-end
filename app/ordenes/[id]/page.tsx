import PreviewDetalleItem from '@/components/PreviewDetalleItem';
import { getOrden } from '@/services/orden';
import dateFormater from '@/utils/dateFormater';
import getTotalOfDetalle from '@/utils/getTotalOfDetalle';
import React from 'react';
interface IProperties {
  params: {
    id: string;
  };
}

const OrdenPage = async ({ params: { id } }: IProperties) => {
  const resposeOrError = await getOrden(Number(id));

  if ('message' in resposeOrError) {
    const {
      message,
      data: { fechaHora, cliente, detalles },
    } = resposeOrError;
    return (
      <div className="flex flex-col gap-2">
        <div className=" border-2 border-theme-border-color p-2">
          <div className="w-full text-center">
            <span className="font-bold underline">Informacion de orden</span>
          </div>
          <div className="flex flex-row justify-between">
            <span>Orden #{id}</span>
            <span>{`${dateFormater(fechaHora.date).timeString} ${
              dateFormater(fechaHora.date).dateString
            }`}</span>
          </div>
        </div>
        <div className="border-2 border-theme-border-color p-2">
          <div className="w-full text-center">
            <span className="font-bold underline">Informacion del cliente</span>
          </div>
          <div className="flex flex-col sm:flex-row">
            <div className="flex flex-col flex-[2]">
              <span className="font-bold">Nombre:</span>
              <span>
                {cliente.nombre} {cliente.apellido}
              </span>
            </div>
            <div className="flex flex-col flex-[3]">
              <span className="font-bold">Direccion:</span>
              <span>{cliente.departamento}</span>
              <span>{cliente.municipio}</span>
              <span>{cliente.direccion}</span>
            </div>
          </div>
        </div>
        <div className="border-2 border-theme-border-color p-2">
          <div className="w-full text-center">
            <span className="font-bold underline">Detalle</span>
          </div>
          <div className="flex flex-col gap-2">
            {detalles
              .sort((detalleA, detalleB) => detalleB.id - detalleA.id)
              .map((detalle) => (
                <PreviewDetalleItem key={detalle.id} {...detalle} />
              ))}
          </div>
          <div className="flex flex-row justify-end p-2">
            <span className="font-bold text-xl">
              Total: {`$${getTotalOfDetalle(detalles)}`}
            </span>
          </div>
        </div>
      </div>
    );
  } else {
    const { error, code } = resposeOrError;
    throw new Error(error);
  }
};

export default OrdenPage;
