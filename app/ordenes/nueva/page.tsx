'use client';

import SelectWithSearch from '@/components/SelectWithSearch';
import { ICliente, IDetalle, IProducto } from '@/types/apiResponse';
import React, { ChangeEvent, useEffect, useMemo, useState } from 'react';
import debounce from 'lodash/debounce';
import PreviewDetalleItem from '@/components/PreviewDetalleItem';
import getTotalOfDetalle from '@/utils/getTotalOfDetalle';
import { validateCantidad } from '@/utils/validations';
import { getClientesByNombreApellido } from '@/services/cliente';
import { getProductoByIdNombre } from '@/services/producto';
import { createOrden } from '@/services/orden';
import { useRouter } from 'next/navigation';
import { ROUTES } from '@/utils/constants';
import Loading from '@/components/Loading';
import Alert from '@/components/Alert';

type TClienteSimple = Omit<
  ICliente,
  'direccion' | 'municipio' | 'departamento'
>;

interface IOrden {
  cliente: number;
  productos: {
    id: number;
    cantidad: number;
  }[];
}

type TDetalleSimple = Omit<IDetalle, 'id'>;

const NuevaOrdenPage = () => {
  const [clientes, setClientes] = useState<TClienteSimple[]>([]);
  const [nombreApellido, setNombreApellido] = useState('');
  const [cliente, setCliente] = useState<TClienteSimple>();
  const [productos, setProductos] = useState<IProducto[]>([]);
  const [detalles, setDetalles] = useState<TDetalleSimple[]>([]);
  const [idNombre, setIdNombre] = useState('');
  const [producto, setProducto] = useState<IProducto>();
  const [cantidad, setCantidad] = useState(1);
  const [loading, setloading] = useState(0);
  const [showCreateConfirm, setShowCreateConfirm] = useState(false);
  const [showSuccessAlert, setShowSuccessAlert] = useState(false);
  const [showAlert, setShowAlert] = useState<string>();
  const [detalleToDelete, setDetalleToDelete] = useState<TDetalleSimple>();

  const router = useRouter();

  const handleNombreApellidoChange = (event: ChangeEvent<HTMLInputElement>) => {
    setNombreApellido(event.currentTarget.value);
  };

  const handleClienteSelect = (id: number | string) => {
    const clienteConId = clientes.find((cliente) => cliente.id === id);
    setCliente(clienteConId);
  };

  const handleClienteUnSelect = () => {
    // eslint-disable-next-line unicorn/no-useless-undefined
    setCliente(undefined);
  };

  const handleIdNombreChange = (event: ChangeEvent<HTMLInputElement>) => {
    setIdNombre(event.currentTarget.value);
  };

  const handleProductoSelect = (id: number | string) => {
    const productoConID = productos.find((producto) => producto.id === id);
    setProducto(productoConID);
  };

  const handleProductoUnSelect = () => {
    setCantidad(1);
    // eslint-disable-next-line unicorn/no-useless-undefined
    setProducto(undefined);
  };

  const handleCantidadChange = (event: ChangeEvent<HTMLInputElement>) => {
    let newCantidad = Math.round(Number(event.currentTarget.value));
    setCantidad(validateCantidad(newCantidad));
  };

  const updateDetalleCantidad = (idProducto: number, nuevaCantidad: number) => {
    const productoIndex = detalles.findIndex(
      (detalle) => detalle.producto.id === idProducto,
    );
    if (productoIndex !== -1) {
      setDetalles((previousDetalles) => {
        const newDetalles = [...previousDetalles];
        newDetalles[productoIndex].cantidad = nuevaCantidad;
        return newDetalles;
      });
    }
  };

  const handleAddProducto = () => {
    if (producto !== undefined) {
      const newSimpleDetalle = {
        producto,
        cantidad,
      };
      setDetalles((previousDetalles) => [
        ...previousDetalles,
        newSimpleDetalle,
      ]);
      handleProductoUnSelect();
    }
  };

  const handleRemoveProducto = (id: number) => {
    setDetalles((previousDetalles) => {
      const newDetalles = previousDetalles.filter(
        (previousDetalle) => previousDetalle.producto.id !== id,
      );

      return newDetalles;
    });
    // eslint-disable-next-line unicorn/no-useless-undefined
    setDetalleToDelete(undefined);
  };

  const debouncedGetClientesWithNombreApellido = useMemo(
    () =>
      debounce(async (nombreApellido: string) => {
        setloading((loading) => loading + 1);
        const responseOrError =
          await getClientesByNombreApellido(nombreApellido);
        setloading((loading) => loading - 1);
        if (responseOrError !== undefined && 'message' in responseOrError) {
          const { data } = responseOrError;
          setClientes(data);
        } else {
          setShowAlert(responseOrError.error);
        }
      }, 500),
    [],
  );

  const debouncedGetProductosWithIdNombre = useMemo(
    () =>
      debounce(async (idNombre: string, ignore: string) => {
        setloading((loading) => loading + 1);
        const responseOrError = await getProductoByIdNombre(idNombre, ignore);
        setloading((loading) => loading - 1);
        if ('message' in responseOrError) {
          const { data } = responseOrError;
          setProductos(data);
        } else {
          setShowAlert(responseOrError.error);
        }
      }, 500),
    [],
  );

  const handleCreateClick = async () => {
    setShowCreateConfirm(false);
    if (cliente && detalles.length > 0) {
      const orden = {
        cliente: cliente.id,
        productos: detalles.map((detalle) => ({
          id: detalle.producto.id,
          cantidad: detalle.cantidad,
        })),
      };
      setloading((loading) => loading + 1);
      const responseOrError = await createOrden(orden);
      setloading((loading) => loading - 1);
      if ('message' in responseOrError) {
        setShowSuccessAlert(true);
      } else {
        setShowAlert(responseOrError.error);
      }
    }
  };

  const handleOpenCreateConfirm = () => {
    setShowCreateConfirm(true);
  };

  const handleCloseCreateConfirm = () => {
    setShowCreateConfirm(false);
  };

  const handleCloseAlert = () => {
    // eslint-disable-next-line unicorn/no-useless-undefined
    setShowAlert(undefined);
  };

  const handleCloseSuccessAlert = () => {
    router.push(ROUTES.ORDENES);
  };

  const handleOpenDetalleToDelete = (detalle: TDetalleSimple) => {
    setDetalleToDelete(detalle);
  };

  const handleAcceptDetalleToDelete = () => {
    if (detalleToDelete !== undefined) {
      handleRemoveProducto(detalleToDelete.producto.id);
    }
  };

  const handleCloseDetalleToDelete = () => {
    // eslint-disable-next-line unicorn/no-useless-undefined
    setDetalleToDelete(undefined);
  };

  useEffect(() => {
    debouncedGetClientesWithNombreApellido(nombreApellido);
  }, [debouncedGetClientesWithNombreApellido, nombreApellido]);

  useEffect(() => {
    debouncedGetProductosWithIdNombre(
      idNombre,
      detalles.map((detalle) => detalle.producto.id).join(','),
    );
  }, [debouncedGetProductosWithIdNombre, detalles, idNombre]);

  return (
    <div>
      <Loading show={loading > 0} />
      <Alert
        show={showCreateConfirm}
        title="¿Terminar orden?"
        message="Una vez que se termine la orden no podra ser modificada."
        onAccept={handleCreateClick}
        onCancel={handleCloseCreateConfirm}
        onClickOutside={handleCloseCreateConfirm}
      />
      <Alert
        show={showAlert !== undefined}
        title="Error"
        message={showAlert ?? ''}
        onAccept={handleCloseAlert}
        onClickOutside={handleCloseAlert}
      />
      <Alert
        show={showSuccessAlert}
        title="Operacion exitosa."
        message={'Se registro la orden con exito.'}
        onAccept={handleCloseSuccessAlert}
        onClickOutside={handleCloseSuccessAlert}
      />
      <Alert
        show={detalleToDelete !== undefined}
        title="Confirmar eliminacion."
        message={`¿Esta seguro que sea eliminar el producto "${detalleToDelete?.producto.nombre}" de la orden?.`}
        onAccept={handleAcceptDetalleToDelete}
        onCancel={handleCloseDetalleToDelete}
        onClickOutside={handleCloseDetalleToDelete}
      />

      <h1>Nueva orden</h1>
      <div>
        <label htmlFor="cliente">Cliente:</label>
        <SelectWithSearch
          autoComplete="off"
          id="cliente"
          onClickOption={handleClienteSelect}
          value={nombreApellido}
          onChange={handleNombreApellidoChange}
          options={clientes.map((cliente) => ({
            id: cliente.id,
            label: `${cliente.id} - ${cliente.nombre} ${cliente.apellido}`,
          }))}
          selectedOption={
            cliente === undefined
              ? undefined
              : {
                  id: cliente.id,
                  label: `${cliente.id} - ${cliente.nombre} ${cliente.apellido}`,
                }
          }
          onUnselect={handleClienteUnSelect}
        />
      </div>
      <div className="flex flex-col sm:flex-row gap-2 sm:items-end">
        <div>
          <label htmlFor="producto">Producto:</label>
          <SelectWithSearch
            autoComplete="off"
            className="sm:w-96"
            id="producto"
            onClickOption={handleProductoSelect}
            value={idNombre}
            onChange={handleIdNombreChange}
            options={productos.map((producto) => ({
              id: producto.id,
              label: `${producto.id} - ${producto.nombre} - $${producto.precio}`,
            }))}
            selectedOption={
              producto === undefined
                ? undefined
                : {
                    id: producto.id,
                    label: `${producto.id} - ${producto.nombre} - $${producto.precio}`,
                  }
            }
            onUnselect={handleProductoUnSelect}
          />
        </div>
        {producto === undefined ? undefined : (
          <>
            <div className="flex flex-col sm:items-end">
              <label htmlFor="cantidad">{`Cantidad (${producto.unidadMedida}):`}</label>
              <input
                type="number"
                value={cantidad}
                onChange={handleCantidadChange}
                className="w-16"
                max={999}
                min={1}
              />
            </div>
            <button
              className="self-center sm:self-auto"
              onClick={handleAddProducto}
            >
              Agregar a la orden
            </button>
          </>
        )}
      </div>
      <div className="flex flex-col gap-2 my-2">
        {detalles.map((detalle) => {
          const handleDetalleCantidadChange = (
            event: ChangeEvent<HTMLInputElement>,
          ) => {
            let newCantidad = Math.round(Number(event.currentTarget.value));
            updateDetalleCantidad(
              detalle.producto.id,
              validateCantidad(newCantidad),
            );
          };
          return (
            <div
              key={detalle.producto.id}
              className="flex flex-row items-center gap-2"
            >
              <div className="flex-1">
                <PreviewDetalleItem
                  id={detalle.producto.id}
                  onChange={handleDetalleCantidadChange}
                  {...detalle}
                />
              </div>
              <span
                onClick={() => handleOpenDetalleToDelete(detalle)}
                className="text-2xl hover:text-theme-red hover:cursor-pointer"
                title="Eliminar producto de la orden"
              >
                ×
              </span>
            </div>
          );
        })}
        <div className="flex flex-row justify-end p-2">
          <span className="font-bold text-xl">
            Total: {`$${getTotalOfDetalle(detalles)}`}
          </span>
        </div>
        {cliente && detalles.length > 0 ? (
          <button className="self-center" onClick={handleOpenCreateConfirm}>
            Terminar orden
          </button>
        ) : undefined}
      </div>
    </div>
  );
};

export default NuevaOrdenPage;
