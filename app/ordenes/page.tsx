import OrdenPreview from '@/components/OrdenPreview';
import Pagination from '@/components/Pagination';
import { getOrdenes } from '@/services/orden';
import { ROUTES } from '@/utils/constants';
import dateFormater from '@/utils/dateFormater';
import Link from 'next/link';
import { notFound } from 'next/navigation';
import React from 'react';

interface IProperties {
  searchParams: {
    page: string;
  };
}

const OrdenesPage = async ({ searchParams: { page } }: IProperties) => {
  const responseOrError = await getOrdenes(page);
  if ('data' in responseOrError) {
    const { ordenes, totalPages } = responseOrError.data;
    if (page === undefined || Number(page) <= totalPages) {
      return (
        <div className="flex flex-col items-center gap-2">
          <h1 className="font-bold">Ordenes</h1>
          <div className="flex flex-row justify-end w-full">
            <Link href={ROUTES.NUEVA_ORDEN} className="boton-default">
              Nueva orden
            </Link>
          </div>
          <div className="flex flex-col gap-2 w-full">
            {ordenes.map((orden) => {
              const { dateString, timeString } = dateFormater(
                orden.fechaHora.date,
              );
              return (
                <Link key={orden.id} href={`${ROUTES.ORDENES}/${orden.id}`}>
                  <OrdenPreview
                    id={orden.id}
                    cliente={`${orden.cliente.nombre} ${orden.cliente.apellido}`}
                    fechaHora={`${timeString} ${dateString}`}
                  />
                </Link>
              );
            })}
          </div>
          <Pagination
            totalPages={totalPages}
            currentPage={page ? Number(page) : 1}
            route={ROUTES.ORDENES}
          />
        </div>
      );
    } else {
      notFound();
    }
  } else {
    throw new Error(responseOrError.error);
  }
};

export default OrdenesPage;
