import type { Metadata } from 'next';
import { Inter } from 'next/font/google';
import './globals.css';
import Header from '@/components/Header';

const inter = Inter({ subsets: ['latin'] });

export const metadata: Metadata = {
  title: 'Prueba tecnica',
  description:
    'Aplicacion desarrollada con el objetivo de servir como una prueba tecnica.',
  authors: {
    name: 'Diego Quinteros',
    url: 'https://diegoquinteros.dev',
  },
};

const RootLayout = ({ children }: { children: React.ReactNode }) => {
  return (
    <html lang="es">
      <body className={`${inter.className} min-h-screen flex flex-col`}>
        <Header />
        <main className="bg-white max-w-5xl my-4 m-auto w-full flex-1 rounded-md border-2 border-theme-border-color shadow-lg shadow-gray-300 p-8">
          {children}
        </main>
      </body>
    </html>
  );
};

export default RootLayout;
