'use client';

import { useRouter } from 'next/navigation';

const ErrorPage = ({ error }: { error: Error & { digest?: string } }) => {
  const router = useRouter();

  return (
    <div className="flex flex-col gap-2 items-center my-8">
      <h1>{error.message}</h1>
      <button onClick={() => window.location.reload()}>
        Intentar de nuevo
      </button>
    </div>
  );
};

export default ErrorPage;
