import React from 'react';

const NotFound = () => {
  return (
    <div>
      <h1 className="my-8 text-center">No se encontro la pagina.</h1>
    </div>
  );
};

export default NotFound;
