import Link from 'next/link';

export default function Home() {
  return (
    <div>
      <h1 className="font-bold">Prueba tecnica</h1>
      <p>
        Esta aplicacion se desarrollo como una prueba tecnica para demostrar las
        habilidades en el desarrollo de aplicaciones usando React.
      </p>
      <h2 className="font-bold">Autor</h2>
      <Link
        target="_blank"
        className="hover:text-blue-500 hover:cursor-pointer hover:underline"
        href="https://diegoquinteros.dev"
      >
        Diego Quinteros
      </Link>
      <h2 className="font-bold">Stack de tecnologias</h2>
      <h3 className="font-bold">Front-end</h3>
      <ul className="list-inside list-disc">
        <li>NextJS</li>
        <li>React</li>
        <li>TypeScript</li>
        <li>Axios</li>
        <li>TailwindCSS</li>
        <li>ESLint</li>
        <li>Prettier</li>
      </ul>
      <h3 className="font-bold">Back-end</h3>
      <ul className="list-inside list-disc">
        <li>Symfony</li>
        <li>Doctrine</li>
        <li>MariaDB</li>
      </ul>
    </div>
  );
}
