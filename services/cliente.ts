import { ICliente, IResponse } from '@/types/apiResponse';
import customAxios from '@/utils/customAxios';
import handleErrors from './handleErrors';

type TClienteSimple = Omit<
  ICliente,
  'direccion' | 'municipio' | 'departamento'
>;

export const getClientesByNombreApellido = async (nombreApellido: string) => {
  try {
    const response = await customAxios.get<IResponse<TClienteSimple[]>>(
      `/cliente?value=${nombreApellido}`,
    );
    return response.data;
  } catch (error) {
    return handleErrors(error);
  }
};
