import { IOrden, IResponse } from '@/types/apiResponse';
import customAxios from '@/utils/customAxios';
import handleErrors from './handleErrors';

interface IData {
  totalPages: number;
  total: number;
  ordenes: IOrden[];
}

interface INewOrden {
  cliente: number;
  productos: {
    id: number;
    cantidad: number;
  }[];
}

export const getOrdenes = async (page: string) => {
  try {
    const response = await customAxios.get<IResponse<IData>>(
      `/orden?page=${page ?? 1}`,
    );
    return response.data;
  } catch (error) {
    return handleErrors(error);
  }
};

export const getOrden = async (id: number) => {
  try {
    const response = await customAxios.get<IResponse<IOrden>>(`/orden/${id}`);
    return response.data;
  } catch (error) {
    return handleErrors(error);
  }
};

export const createOrden = async (orden: INewOrden) => {
  try {
    const response = await customAxios.post<IResponse<undefined>>(
      '/orden',
      orden,
    );
    return response.data;
  } catch (error) {
    return handleErrors(error);
  }
};
