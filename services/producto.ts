import { IProducto, IResponse } from '@/types/apiResponse';
import customAxios from '@/utils/customAxios';
import handleErrors from './handleErrors';

export const getProductoByIdNombre = async (
  idNombre: string,
  ignore: string,
) => {
  try {
    const response = await customAxios.get<IResponse<IProducto[]>>(
      `/producto?value=${idNombre}&ignore=${ignore}`,
    );
    return response.data;
  } catch (error) {
    return handleErrors(error);
  }
};
