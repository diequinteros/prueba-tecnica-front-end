import { isAxiosError } from 'axios';

interface IServiceError {
  error: string;
  code?: number;
}

const handleErrors = (error: any): IServiceError => {
  if (isAxiosError(error)) {
    return {
      error: error.response?.data ?? error.message,
      code: Number(error.code),
    };
  }

  return { error: 'Error inesperado al hacer la peticion' };
};

export default handleErrors;
