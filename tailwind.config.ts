import type { Config } from 'tailwindcss';
import colors from 'tailwindcss/colors';

const config: Config = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx,mdx}',
    './components/**/*.{js,ts,jsx,tsx,mdx}',
    './app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    extend: {
      backgroundImage: {
        'gradient-radial': 'radial-gradient(var(--tw-gradient-stops))',
        'gradient-conic':
          'conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))',
      },
      colors: {
        'theme-dark-blue': '#0663BA',
        'theme-blue': '#66B2FF',
        'theme-green': '#00CC66',
        'theme-red': '#FF6666',
        'theme-border-color': colors.gray[200],
        'theme-orange': colors.orange[500],
      },
    },
  },
  plugins: [],
};
export default config;
